const express = require('express');
const cors = require('cors')
const app = express();
app.use(cors())
const port = 5000


// const variables = require('./config')
const { Connection, Request } = require("tedious");

// Create connection to database
const config = {
  authentication: {
    options: {
      userName: "telstralab",
      password: "Telstra1234"
    },
    type: "default"
  },
  server: "smart-space-server.database.windows.net",
  options: {
    database: "SmartSpaceDatabase",
    encrypt: true
  }
};
const connection = new Connection(config);
// Attempt to connect and execute queries if connection goes through
connection.on("connect", err => {
  if (err) {
    console.error(err.message);
  } else {
    app.get('/azureSQLGetAllEast', (req, res) => {
        queryContainerAllItemsEast().then((data)=>{
          res.send(data);
        })
    });
    app.get('/azureSQLGetAllWest', (req, res) => {
        queryContainerAllItemsWest().then((data)=>{
          res.send(data);
        })
    });
    app.get('/azureSQLGetAllNorthWest', (req, res) => {
        queryContainerAllItemsNorthWest().then((data)=>{
          res.send(data);
        })
    });
    app.get('/azureSQLGetAllSouthWest', (req, res) => {
        queryContainerAllItemsSouthWest().then((data)=>{
          res.send(data);
        })
    });
  }
});

function queryContainerAllItemsEast() {
  var database = []
  // Read all rows from table
  const request = new Request(
    `SELECT * FROM levelTwoEast`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
      } else {
//        console.log(database)
      }
    }
  );
  const promise = new Promise(function(resolve, reject) {
    request.on("row", columns => {
        var rowDict = {}
        columns.forEach(column => {
        rowDict[column.metadata.colName] = column.value
        });
        database.push(rowDict)
        resolve(database);
    });
  });
  connection.execSql(request)
  return promise;
}

function queryContainerAllItemsWest() {
  var database = []
  // Read all rows from table
  const request = new Request(
    `SELECT * FROM levelTwoWest`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
      } else {
//        console.log(database)
      }
    }
  );
  const promise = new Promise(function(resolve, reject) {
    request.on("row", columns => {
        var rowDict = {}
        columns.forEach(column => {
        rowDict[column.metadata.colName] = column.value
        });
        database.push(rowDict)
        resolve(database);
    });
  });
  connection.execSql(request)
  return promise;
}

function queryContainerAllItemsNorthWest() {
  var database = []
  // Read all rows from table
  const request = new Request(
    `SELECT * FROM levelTwoNorthWest`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
      } else {
//        console.log(database)
      }
    }
  );
  const promise = new Promise(function(resolve, reject) {
    request.on("row", columns => {
        var rowDict = {}
        columns.forEach(column => {
        rowDict[column.metadata.colName] = column.value
        });
        database.push(rowDict)
        resolve(database);
    });
  });
  connection.execSql(request)
  return promise;
}

function queryContainerAllItemsSouthWest() {
  var database = []
  // Read all rows from table
  const request = new Request(
    `SELECT * FROM southwest`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
      } else {
//        console.log(database)
      }
    }
  );
  const promise = new Promise(function(resolve, reject) {
    request.on("row", columns => {
        var rowDict = {}
        columns.forEach(column => {
        rowDict[column.metadata.colName] = column.value
        });
        database.push(rowDict)
        resolve(database);
    });
  });
  connection.execSql(request)
  return promise;
}



app.listen(port, () => console.log(`Example app listening on port ${port}!`))
